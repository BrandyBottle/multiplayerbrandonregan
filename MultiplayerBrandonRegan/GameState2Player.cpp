#include "GameState2Player.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include "MusicPlayer.hpp"


GameState2Player::GameState2Player(StateStack& stack, Context context) 
	:State(stack, context)
	, mWorld(*context.window, *context.fonts, *context.sounds, 2)
	, mPlayer1(*context.player1)
	, mPlayer2(*context.player2)
{

	

	//Play the mission theme
	context.music->play(MusicIDs::MissionTheme);
}

void GameState2Player::draw()
{
	mWorld.draw();
}

bool GameState2Player::update(sf::Time dt)
{
	mWorld.update(dt);

	if (mWorld.hasAlivePlayer())
	{
		mDeathStateDelay += dt;
		if (mDeathStateDelay > sf::seconds(2))
		{
			requestStackPush(StateIDs::GameOver);
		}
	}

	else
	{
		CommandQueue& commands = mWorld.getCommandQueue();
		mPlayer1.handleRealtimeInput(commands);
		mPlayer2.handleRealtimeInput(commands);
	}
	

	return true;
}

bool GameState2Player::handleEvent(const sf::Event & event)
{
	//Game input handling
	CommandQueue& commands = mWorld.getCommandQueue();
	mPlayer1.handleEvent(event, commands);
	mPlayer2.handleEvent(event, commands);

	//Escape pressed, trigger pause screen
	if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
	{
		requestStackPush(StateIDs::Pause);
	}
	return true;
}


