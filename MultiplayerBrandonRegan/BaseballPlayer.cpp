#include "BaseballPlayer.hpp"
#include "DataTables.hpp"
#include "Utility.hpp"
#include "CommandQueue.hpp"
#include "ResourceHolder.hpp"
#include "SoundNode.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

#include <cmath>


namespace
{
	const std::vector<BaseballPlayerData> Table = initializeBaseballPlayerData();
}

BaseballPlayer::BaseballPlayer(Type type, const TextureHolder& textures, const FontHolder& fonts, SoundPlayer& sounds)
	: Entity(Table[static_cast<int>(type)].hitpoints)
	, mType(type)
	, mSprite(textures.get(Table[static_cast<int>(type)].texture), Table[static_cast<int>(type)].textureRect)
	, mExplosion(textures.get(TextureIDs::Explosion))
	, mIsSwinging(false)
	, mIsFacingLeft(false)
	, mShowExplosion(true)
	, mDefeated(false)
	, mPlayedExplosionSound(false)
	, mPlayedSwingSound(false)
	, mSwingCooldown(sf::Time::Zero)
	, mSounds(sounds)
{

	mSprite.setScale(sf::Vector2f(3, 3));
	centreOrigin(mSprite);
	

	mExplosion.setScale(2, 2);
	mExplosion.setFrameSize(sf::Vector2i(120, 120));
	mExplosion.setNumFrames(23);
	mExplosion.setDuration(sf::seconds(1.25f));
	centreOrigin(mExplosion);

	updateTexts();
}

unsigned int BaseballPlayer::getCategory() const
{
	if (mType == Type::Player1)
	{
		return static_cast<int>(Category::Player1);
	}
	else
	{
		return static_cast<int>(Category::Player2);
	}
	
}

sf::FloatRect BaseballPlayer::getBoundingRect() const
{
	sf::FloatRect rect = getWorldTransform().transformRect(mSprite.getGlobalBounds());

	if (!mIsSwinging)
	{
		rect.width = rect.width / 4;
		rect.height = rect.height / 2;

		rect.top = rect.top + getWorldTransform().transformRect(mSprite.getGlobalBounds()).height / 8.f;
		rect.left = rect.left + getWorldTransform().transformRect(mSprite.getGlobalBounds()).width / 2.f;
	}
	

	// Still not the best but player hit boxes are better

	return rect;
}

void BaseballPlayer::defeated()
{
	// Player is defeated
	mDefeated = true;
}

void BaseballPlayer::swing()
{
	if (!mIsSwinging)
	{
		mIsSwinging = true;
		sf::IntRect textureRect(5+(static_cast<int>(mType)*61.f)+ 27, 5, 24, 24);
		mSprite.setTextureRect(textureRect);
		mSounds.play(SoundEffectIDs::Swing);
	}

}

void BaseballPlayer::playSwingSound(CommandQueue & commands)
{
	//playLocalSound(commands, SoundEffectIDs::BatHit);
}

void BaseballPlayer::updateCurrent(sf::Time dt, CommandQueue & commands)
{
	if (mDefeated)
	{
		mExplosion.update(dt);
		if (!mPlayedExplosionSound)
		{
			mSounds.play(SoundEffectIDs::Explosion3);

			mPlayedExplosionSound = true;
		}
	}
	if (mIsSwinging)
	{
		if (mSwingCooldown < sf::seconds(.25f))
		{
			mSwingCooldown += dt;
		}
		else
		{
			mIsSwinging = false;
			sf::IntRect textureRect(5+(static_cast<int>(mType)*61.f), 5, 17, 24);
			mSprite.setTextureRect(textureRect);
			mSwingCooldown = sf::Time::Zero;
		}
	}
	checkSwingInterval(dt);
}

void BaseballPlayer::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (mDefeated)
	{
		target.draw(mExplosion, states);
	}
	else
	{
		target.draw(mSprite, states);
	}
	
}
void BaseballPlayer::updateTexts()
{
	/*mHealthDisplay->setString(std::to_string(getHitpoints()) + " HP");
	mHealthDisplay->setPosition(0.f, 50.f);
	mHealthDisplay->setRotation(-getRotation());

	if (mMissileDisplay)
	{
		if (mMissileAmmo == 0)
			mMissileDisplay->setString("");
		else
			mMissileDisplay->setString("M: " + std::to_string(mMissileAmmo));
	}*/
}

void BaseballPlayer::checkSwingInterval(sf::Time dt)
{
	/*if (mIsSwinging)
	{

	}

		&& mSwingCooldown <= sf::Time::Zero)
	{
		mSwingCooldown += Table[static_cast<int>(mType)].swingInterval;
		mIsSwinging = false;
		sf::IntRect textureRect((static_cast<int>(mType)*40.f), 0, 17, 24);
		mSprite.setTextureRect(textureRect);
		
	}
	else if(mSwingCooldown > sf::Time::Zero)
	{
		mSwingCooldown -= dt;
	}*/
}


void BaseballPlayer::playLocalSound(CommandQueue& commands, SoundEffectIDs effect)
{
	sf::Vector2f worldPosition = getWorldPosition();

	Command command;
	command.category = static_cast<int>(Category::SoundEffect);
	command.action = derivedAction<SoundNode>([effect, worldPosition](SoundNode& node, sf::Time)
	{
		node.playSound(effect, worldPosition);
	});
	commands.push(command);

}
