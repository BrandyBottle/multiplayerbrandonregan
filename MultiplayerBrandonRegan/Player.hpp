#pragma once
#include "Command.hpp"
#include "SFML/Window/Event.hpp"
#include <map>

class CommandQueue;

class Player
{
public:
	enum class Action{MoveLeft, MoveRight, MoveUp, MoveDown, Swing, ActionCount};
	enum class MissionStatus{MissionRunning, MissionSuccess, MissionFailure};

public:
	Player(int playerNumber);
	void handleEvent(const sf::Event& event, CommandQueue& commands);
	void handleRealtimeInput(CommandQueue& commands);
	void assignKey(Action action, sf::Keyboard::Key key);
	sf::Keyboard::Key getAssignedKey(Action action) const;

	void setMissionStatus(MissionStatus status);
	MissionStatus getMissionStatus() const;

private:
	void initializeP1Controls();
	void initializeP2Controls();
	void initializeActions();
	static bool isRealtimeAction(Action action);

private:
	std::map<sf::Keyboard::Key, Action> mKeyBinding;
	std::map<Action, Command> mActionBinding;
	MissionStatus mCurrentMissionStatus;

};