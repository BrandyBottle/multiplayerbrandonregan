#include "ChallengerApproachingState.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"
#include "MusicPlayer.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>

ChallengerApproachingState::ChallengerApproachingState(StateStack & stack, Context context)
	:State(stack, context)
	, mSounds(*context.sounds)
	, mElapsedTime(sf::Time::Zero)
{
	sf::Font& font = context.fonts->get(FontIDs::Main);
	sf::Vector2f windowSize(context.window->getSize());

	mText.setFont(font);
	mText.setString("Challenger Approaching!");
	mText.setCharacterSize(35);
	centreOrigin(mText);
	mText.setPosition(0.5f * windowSize.x, 0.5f * windowSize.y);

	context.music->play(MusicIDs::NewChallenger);
}

void ChallengerApproachingState::draw()
{
	sf::RenderWindow& window = *getContext().window;
	window.setView(window.getDefaultView());

	sf::RectangleShape backgroundShape;
	backgroundShape.setFillColor(sf::Color(0, 0, 0, 150));
	backgroundShape.setSize(window.getView().getSize());

	window.draw(backgroundShape);
	window.draw(mText);
}

bool ChallengerApproachingState::update(sf::Time dt)
{
	if (mElapsedTime < sf::seconds(3))
	{
		mElapsedTime += dt;
	}
	else
	{
		requestStackClear();
		requestStackPush(StateIDs::Game2);
	}
	return false;
}

bool ChallengerApproachingState::handleEvent(const sf::Event & event)
{
	return false;
}
