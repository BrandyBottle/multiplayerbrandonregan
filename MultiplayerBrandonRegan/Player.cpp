#include "Player.hpp"
#include "CommandQueue.hpp"
#include "BaseballPlayer.hpp"

#include <map>
#include <string>
#include <algorithm>

using namespace std::placeholders;

struct PlayerMover
{
	PlayerMover(float vx, float vy, bool vertical) : velocity(vx, vy), verticalMove(vertical)
	{

	}
	void operator() (BaseballPlayer& player, sf::Time) const
	{
		sf::Vector2f move(velocity.x * 5, velocity.y * 5);
		player.setPosition(player.getPosition() + move);
		if (!verticalMove)
		{
			player.setScale(velocity.x, 1.f);
			if (velocity.x == -1)
			{
				player.mIsFacingLeft = true;
			}
			else if (velocity.x == 1)
			{
				player.mIsFacingLeft = false;
			}
		}
	}
	sf::Vector2f velocity;
	bool verticalMove;
};

Player::Player(int playerNumber)
{
	if (playerNumber == 1)
	{
		initializeP1Controls();
	}
	else if (playerNumber == 2)
	{
		initializeP2Controls();
	}
		
}

void Player::handleEvent(const sf::Event& event, CommandQueue& commands)
{
	if (event.type == sf::Event::KeyPressed)
	{
		//check if key pressed is in the key bindings, if so trigger command
		auto found = mKeyBinding.find(event.key.code);
		if (found != mKeyBinding.end() && !isRealtimeAction(found->second))
		{
			commands.push(mActionBinding[found->second]);
		}
	}
}

void Player::handleRealtimeInput(CommandQueue& commands)
{
	//Check if any key binding keys are pressed
	for (auto pair : mKeyBinding)
	{
		if (sf::Keyboard::isKeyPressed(pair.first) && isRealtimeAction(pair.second))
		{
			commands.push(mActionBinding[pair.second]);
		}
	}
}

void Player::assignKey(Action action, sf::Keyboard::Key key)
{
	//Remove all keys that are already mapped to an action
	for (auto itr = mKeyBinding.begin(); itr != mKeyBinding.end();)
	{
		if (itr->second == action)
		{
			mKeyBinding.erase(itr++);
		}
		else
		{
			++itr;
		}
		//insert new binding
		mKeyBinding[key] = action;
	}
}

sf::Keyboard::Key Player::getAssignedKey(Action action) const
{
	for (auto pair : mKeyBinding)
	{
		if (pair.second == action)
		{
			return pair.first;
		}
	}
	return sf::Keyboard::Unknown;
}

void Player::setMissionStatus(MissionStatus status)
{
	mCurrentMissionStatus = status;
}

void Player::initializeP1Controls()
{
	mKeyBinding[sf::Keyboard::Left] = Action::MoveLeft;
	mKeyBinding[sf::Keyboard::Right] = Action::MoveRight;
	mKeyBinding[sf::Keyboard::Up] = Action::MoveUp;
	mKeyBinding[sf::Keyboard::Down] = Action::MoveDown;
	mKeyBinding[sf::Keyboard::Space] = Action::Swing;

	initializeActions();

	for (auto& pair : mActionBinding)
	{
		pair.second.category = static_cast<unsigned int>(Category::Player1);
	}
}

void Player::initializeP2Controls()
{
	mKeyBinding[sf::Keyboard::A] = Action::MoveLeft;
	mKeyBinding[sf::Keyboard::D] = Action::MoveRight;
	mKeyBinding[sf::Keyboard::W] = Action::MoveUp;
	mKeyBinding[sf::Keyboard::S] = Action::MoveDown;
	mKeyBinding[sf::Keyboard::E] = Action::Swing;

	initializeActions();

	for (auto& pair : mActionBinding)
	{
		pair.second.category = static_cast<unsigned int>(Category::Player2);
	}
}

void Player::initializeActions()
{
	mActionBinding[Action::MoveLeft].action = derivedAction<BaseballPlayer>(PlayerMover(-1, 0.f, false));
	mActionBinding[Action::MoveRight].action = derivedAction<BaseballPlayer>(PlayerMover(1, 0.f, false));
	mActionBinding[Action::MoveUp].action = derivedAction<BaseballPlayer>(PlayerMover(0.f, -1, true));
	mActionBinding[Action::MoveDown].action = derivedAction<BaseballPlayer>(PlayerMover(0.f, 1, true));
	mActionBinding[Action::Swing].action = derivedAction<BaseballPlayer>([](BaseballPlayer& a, sf::Time) { a.swing(); });

}

bool Player::isRealtimeAction(Action action)
{
	switch (action)
	{
	case Action::MoveLeft:
	case Action::MoveRight:
	case Action::MoveUp:
	case Action::MoveDown:
		return true;
	default:
		return false;
	}
}

