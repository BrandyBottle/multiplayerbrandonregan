#include "LevelSelectState.hpp"

#include "Utility.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

LevelSelectState::LevelSelectState(StateStack & stack, Context context)
	: State(stack, context)
	, mGUIContainer()
	, mSelectionBox()
	, levelManager(*context.levelManager)
{
	sf::Texture& texture = context.textures->get(TextureIDs::MenuScreen);
	texture.setRepeated(true);
	mBackgroundSprite.setTexture(texture);

	// Tiles the background image to fill the screen
	mBackgroundSprite.setTextureRect(sf::IntRect(0, 0, 1500, 768));

	mSelectionBox.setFillColor(sf::Color(173, 216, 230));
	mSelectionBox.setOutlineColor(sf::Color(255, 255, 255));
	mSelectionBox.setOutlineThickness(2.f);
	mSelectionBox.setSize(sf::Vector2f(400, 500));
	mSelectionBox.setPosition(332, 90);

	auto game1Button = std::make_shared<GUI::Button>(context);
	game1Button->setPosition(384, 150);
	game1Button->setText("1 Player Game");
	game1Button->setCallback([this]()
	{
		requestStackPop();
		requestStackPush(StateIDs::Game1);
	});

	auto game2Button = std::make_shared<GUI::Button>(context);
	game2Button->setPosition(384, 300);
	game2Button->setText("2 Player Game");
	game2Button->setCallback([this]()
	{
		requestStackPop();
		requestStackPush(StateIDs::Game2);
	});

	auto menuButton = std::make_shared<GUI::Button>(context);
	menuButton->setPosition(384, 450);
	menuButton->setText("Back To Menu");
	menuButton->setCallback([this]()
	{
		requestStackPop();
		requestStackPush(StateIDs::Menu);
	});

	mGUIContainer.pack(game1Button);
	mGUIContainer.pack(game2Button);
	mGUIContainer.pack(menuButton);
}

void LevelSelectState::draw()
{
	sf::RenderWindow& window = *getContext().window;

	window.draw(mBackgroundSprite);
	window.draw(mSelectionBox);
	window.draw(mGUIContainer);
}

bool LevelSelectState::update(sf::Time dt)
{
	if (mBackgroundSprite.getPosition().x <= -100)
	{
		mBackgroundSprite.setPosition(0, 0);
	}
	mBackgroundSprite.setPosition(mBackgroundSprite.getPosition() + sf::Vector2f(-1, 0));
	return false;
}

bool LevelSelectState::handleEvent(const sf::Event & event)
{
	mGUIContainer.handleEvent(event);
	return false;
}
