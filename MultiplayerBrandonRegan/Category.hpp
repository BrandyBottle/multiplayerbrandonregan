#pragma once
//Entity/Scene node category, used to dispatch messages

enum class Category {
	None = 0,
	SceneAirLayer = 1 << 0,
	PlayerAircraft = 1 << 1,
	AlliedAircraft = 1 << 2,
	EnemyAircraft = 1 << 3,
	Player1 = 1 << 4,
	Player2 = 1 << 5,
	Ball = 1 << 6,
	Pickup = 1 << 7,
	AlliedProjectile = 1 << 8,
	EnemyProjectile = 1 << 9,
	ParticleSystem = 1 << 10,
	SoundEffect = 1 << 0,


	Aircraft = PlayerAircraft | AlliedAircraft | EnemyAircraft,
	Projectile = AlliedProjectile | EnemyProjectile,
	BaseballPlayer = Player1 | Player2
};