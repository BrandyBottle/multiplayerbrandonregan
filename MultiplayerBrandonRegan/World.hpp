#pragma once
#include "ResourceHolder.hpp"
#include "ResourceIdentifiers.hpp"
#include "SceneNode.hpp"
#include "SpriteNode.hpp"
#include "Command.hpp"
#include "CommandQueue.hpp"
#include "BloomEffect.hpp"
#include "SoundPlayer.hpp"
#include "BaseballPlayer.hpp"
#include "GameBall.hpp"
#include "LevelManager.hpp"

#include "SFML/System/NonCopyable.hpp"
#include "SFML/Graphics/View.hpp"
#include "SFML/Graphics/Texture.hpp"

#include <array>
#include <queue>

//Forward declaration
namespace sf
{
	class RenderTarget;
}

class World : private sf::NonCopyable {
public:
	explicit World(sf::RenderTarget& outputTarget, FontHolder& fonts, SoundPlayer& sounds, int level);
	void update(sf::Time dt);
	void draw();

	CommandQueue& getCommandQueue();

	bool hasAlivePlayer() const;

private:
	void loadTextures();
	void buildScene();
	void adaptBallPosition();
	void adaptPlayerPosition();
	void updateSpeedDisplay();
	void handleCollisions();
	void updateSounds();

	sf::FloatRect getViewBounds() const;

private:
	enum Layer{Background, LowerAir, UpperAir, LayerCount};

private:
	sf::RenderTarget& mTarget;
	sf::RenderTexture mSceneTexture;
	sf::View mWorldView;
	TextureHolder mTextures;
	FontHolder&	mFonts;
	SoundPlayer& mSounds;

	SceneNode mSceneGraph;
	std::array<SceneNode*, static_cast<int>(Layer::LayerCount)> mSceneLayers;
	CommandQueue mCommandQueue;

	sf::FloatRect mWorldBounds;
	sf::Vector2f mSpawnPosition;
	float mScrollSpeed;
	BaseballPlayer* mBaseballPlayer1;
	BaseballPlayer* mBaseballPlayer2;
	GameBall* mGameBall;
	TextNode* mSpeedDisplay;
	int mLevel;
	bool mPlayerDied;

	BloomEffect	mBloomEffect;
};
