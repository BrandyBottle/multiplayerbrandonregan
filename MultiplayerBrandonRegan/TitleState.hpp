#pragma once
#include "State.hpp"

#include "SFML/Graphics/Sprite.hpp"
#include "SFML/Graphics/Text.hpp"

#include <vector>

class TitleState : public State
{
public:
	TitleState(StateStack& stack, Context context);

	virtual void draw();
	virtual bool update(sf::Time dt);
	virtual bool handleEvent(const sf::Event& event);

private:
	sf::Sprite mBackgroundSprite;
	sf::Sprite mLogo;
	sf::Text mText;

	float mLogoStopPosition;

	bool mTitleDelayFinished;
	sf::Time mDelayTime;


};