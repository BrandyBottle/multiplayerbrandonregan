#pragma once
#include "State.hpp"
#include "Container.hpp"
#include "SoundPlayer.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


class GameOverState : public State
{
public:
	GameOverState(StateStack& stack, Context context);

	virtual void		draw();
	virtual bool		update(sf::Time dt);
	virtual bool		handleEvent(const sf::Event& event);


private:
	sf::Text			mGameOverText1;
	sf::Text			mGameOverText2;
	sf::Time			mElapsedTime;
	sf::RectangleShape	mGameOverBack1;
	sf::RectangleShape	mGameOverBack2;
	SoundPlayer&		mSounds;
	bool				mKOSoundPlayed;
};