#include "State.hpp"
#include "StateStack.hpp"

State::State(StateStack & stack, Context context) 
	: mStack(&stack), mContext(context)
{
}

State::~State()
{
}

void State::requestStackPush(StateIDs stateID)
{
	mStack->pushState(stateID);
}

void State::requestStackPop()
{
	mStack->popState();
}

void State::requestStackClear()
{
	mStack->clearStates();
}

State::Context State::getContext() const
{
	return mContext;
}

State::Context::Context(sf::RenderWindow & window, TextureHolder & textures, FontHolder & fonts, Player & player1, Player & player2, MusicPlayer& music, SoundPlayer& sounds, LevelManager& levelManager)
	: window(&window)
	, textures(&textures)
	, fonts(&fonts)
	, player1(&player1)
	, player2(&player2)
	, music(&music)
	, sounds(&sounds)
	, levelManager(&levelManager)
{
}
