#pragma once
#include "StateIdentifiers.hpp"
#include "ResourceIdentifiers.hpp"


#include "SFML/System/Time.hpp"
#include "SFML/Window/Event.hpp"

#include <memory>

namespace sf
{
	class RenderWindow;
}

class Player;
class StateStack;
class SoundPlayer;
class MusicPlayer;
class LevelManager;

class State
{
public:
	typedef std::unique_ptr<State> Ptr;

	struct Context
	{
		Context(sf::RenderWindow& window, TextureHolder& textures, FontHolder& fonts, Player& player1, Player& player2, MusicPlayer& music, SoundPlayer& sounds, LevelManager& levelManager);
		sf::RenderWindow* window;
		TextureHolder* textures;
		FontHolder* fonts;
		Player* player1;
		Player* player2;
		MusicPlayer* music;
		SoundPlayer* sounds;
		LevelManager* levelManager;
	};

public:
	State(StateStack& stack, Context context);
	virtual ~State();

	virtual void draw() = 0;
	virtual bool update(sf::Time dt) = 0;
	virtual bool handleEvent(const sf::Event& event) = 0;

protected:
	void requestStackPush(StateIDs stateID);
	void requestStackPop();
	void requestStackClear();

	Context getContext() const;

private:
	StateStack* mStack;
	Context mContext;

};