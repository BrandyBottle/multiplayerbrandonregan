#pragma once
#include "State.hpp"
#include "Player.hpp"
#include "Container.hpp"
#include "Button.hpp"
#include "Label.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

#include <array>


class Player2ControlState : public State
{
public:
	Player2ControlState(StateStack& stack, Context context);

	virtual void draw();
	virtual bool update(sf::Time dt);
	virtual bool handleEvent(const sf::Event& event);


private:
	void updateLabels();
	void addButtonLabel(Player::Action action, float y, const std::string& text, Context context);


private:
	sf::Sprite mBackgroundSprite;
	GUI::Container mGUIContainer;
	sf::RectangleShape mSettingsBox;
	std::array<GUI::Button::Ptr, static_cast<int>(Player::Action::ActionCount)>	mBindingButtons;
	std::array<GUI::Label::Ptr, static_cast<int>(Player::Action::ActionCount)> 	mBindingLabels;
};