#pragma once
#include "State.hpp"
#include "Player.hpp"
#include "Container.hpp"
#include "Button.hpp"
#include "Label.hpp"
#include "LevelManager.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

#include <array>


class LevelSelectState : public State
{
public:
	LevelSelectState(StateStack& stack, Context context);

	virtual void draw();
	virtual bool update(sf::Time dt);
	virtual bool handleEvent(const sf::Event& event);


private:
	void addButtonLabel(Player::Action action, float x, float y, const std::string& text, Context context);


private:
	sf::Sprite	mBackgroundSprite;
	GUI::Container mGUIContainer;
	sf::RectangleShape mSelectionBox;
	LevelManager& levelManager;
};