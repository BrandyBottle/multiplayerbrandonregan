#pragma once
class LevelManager
{

public:
	enum class Level { notSet, onePlayerGame, twoPlayerGame, TypeCount };
	
public:
	LevelManager();
	int getCurrentLevel();
	void setLevel(int level);

public:
	Level currentLevel;
};