#include "GameBall.hpp"
#include "DataTables.hpp"
#include "ResourceHolder.hpp"

#include "Utility.hpp"
#include "Constants.hpp"
#include "EmitterNode.hpp"

#include "SFML/Graphics/RenderTarget.hpp"
#include "SFML/Graphics/RenderStates.hpp"

namespace
{
	const std::vector<GameBallData> Table = initializeGameBallData();
}

GameBall::GameBall(const TextureHolder& textures, const FontHolder& fonts, SoundPlayer& sounds)
	:Entity(1)
	, mSprite(textures.get(Table[0].texture))
	, mDirection(-1, 1)
	, mSpeed(0)
	, mHitBuffer(Table[0].hitDelay)
	, mSounds(sounds)
{
	setVelocity(mDirection.x * mSpeed, mDirection.y * mSpeed);

	std::unique_ptr<EmitterNode> propellant(new EmitterNode(Particle::Type::Exhaust));
	propellant->setPosition(getBoundingRect().width / 2.f, getBoundingRect().height / 2.f);
	attachChild(std::move(propellant));
}

sf::FloatRect GameBall::getBoundingRect() const
{
	return getWorldTransform().transformRect(mSprite.getGlobalBounds());
}

unsigned int GameBall::getCategory() const
{
	return static_cast<int>(Category::Ball);
}

int GameBall::getSpeed()
{
	return mSpeed;
}

void GameBall::increaseSpeed()
{
	mSpeed += 5;
	setVelocity(mDirection.x * mSpeed, mDirection.y * mSpeed);
}

void GameBall::reverseXDirection()
{
	mDirection.x = -mDirection.x;
}

void GameBall::reverseYDirection()
{
	mDirection.y = -mDirection.y;
}

void GameBall::setXDirection(float x)
{
	mDirection.x = x;
}

void GameBall::hit()
{
	if (!isHit)
	{
		isHit = true;
		mSounds.play(SoundEffectIDs::BatHit);
		increaseSpeed();
	}
	
}

void GameBall::updateCurrent(sf::Time dt, CommandQueue & commands)
{
	checkHitInterval(dt);
	setVelocity(mDirection.x * (mSpeed * 600) * dt.asSeconds(), mDirection.y * (mSpeed * 600) * dt.asSeconds());
	Entity::updateCurrent(dt, commands);
}

void GameBall::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(mSprite, states);
}

void GameBall::checkHitInterval(sf::Time dt)
{
	if (isHit && mHitBuffer <= sf::Time::Zero)
	{
		mHitBuffer += Table[0].hitDelay;
		isHit = false;
	}
	else if (mHitBuffer > sf::Time::Zero)
	{
		mHitBuffer -= dt;
	}
}

void GameBall::remove()
{
	Entity::remove();
}
