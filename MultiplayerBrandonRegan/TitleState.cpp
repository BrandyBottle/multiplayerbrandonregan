#include "TitleState.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"

#include "SFML/Graphics/RenderWindow.hpp"

TitleState::TitleState(StateStack & stack, Context context)
	: State(stack, context)
	, mText()
	, mDelayTime(sf::Time::Zero)
{
	sf::Vector2f windowSize(context.window->getSize());

	mTitleDelayFinished = false;

	mBackgroundSprite.setTexture(context.textures->get(TextureIDs::TitleScreen));

	mLogo.setTexture(context.textures->get(TextureIDs::TitleLogo));
	mLogo.setPosition(0.52f * windowSize.x, -.30f * windowSize.y);

	centreOrigin(mLogo);

	mLogoStopPosition = 0.35f * windowSize.y;
	
	mText.setFont(context.fonts->get(FontIDs::Main));
	mText.setString("PLAY BALL");
	centreOrigin(mText);
	mText.setPosition(0.5f * windowSize.x, 0.7f * windowSize.y);
	mText.setCharacterSize(35);
}

void TitleState::draw()
{
	sf::RenderWindow& window = *getContext().window;

	window.draw(mBackgroundSprite);
	window.draw(mLogo);

	if (mTitleDelayFinished)
	{
		window.draw(mText);
	}
	
	
}

bool TitleState::update(sf::Time dt)
{
	if (!mTitleDelayFinished)
	{
		if (mLogo.getPosition().y <= mLogoStopPosition)
		{
			sf::Vector2f movePosition = mLogo.getPosition() + sf::Vector2f(0, 12);
			mLogo.setPosition(movePosition);
		}
		else
		{
			mTitleDelayFinished = true;
		}
	}
	else
	{
		mDelayTime += dt;

		if (mDelayTime < sf::seconds(.75f))
		{
			mLogo.setScale(mLogo.getScale() - sf::Vector2f(.003, .003));
		}
		else if(mDelayTime < sf::seconds(1.5f))
		{
			mLogo.setScale(mLogo.getScale() + sf::Vector2f(.003, .003));
		}
		else
		{
			mDelayTime = sf::Time::Zero;
		}

	}
	/*if ()
	{	
		mTransitionTime += dt;
		sf::Vector2f movePosition = mBackgroundSprite.getPosition() + sf::Vector2f(0, -4);
		mBackgroundSprite.setPosition(movePosition);
		if (mTransitionTime > sf::seconds(3.f))
		{
			requestStackPop();
			requestStackPush(StateIDs::Menu);
		}
		
	}*/
	return true;
}

bool TitleState::handleEvent(const sf::Event& event)
{
	//If any key is pressed move to menu state
	if (event.type == sf::Event::KeyPressed)
	{
		//mTransitioning = true;
		//mTransitionTime = sf::Time::Zero;
		requestStackPop();
		requestStackPush(StateIDs::Menu);
	}
	return true;
}
