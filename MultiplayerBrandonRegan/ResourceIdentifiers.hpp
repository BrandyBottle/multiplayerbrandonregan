#pragma once
//Forward declaration of SFML classes
namespace sf
{
	class Texture;
	class Font;
	class Shader;
	class SoundBuffer;
}

enum class TextureIDs{BaseballPlayer, Arena, TitleScreen, Buttons, Explosion, Particle, TitleLogo, MenuScreen, GameBall, MenuLogo};
enum class Shaders{BrightnessPass, DownSamplePass, GaussianBlurPass, AddPass};
enum class FontIDs{Main};
enum class MusicIDs{MenuTheme, MissionTheme, NewChallenger};
enum class SoundEffectIDs{BatHit, Explosion3, KO, Swing, Button};


//Forward declaration and typedef
template <typename Resource, typename Identifier>
class ResourceHolder;

typedef ResourceHolder<sf::Texture, TextureIDs> TextureHolder;
typedef ResourceHolder<sf::Font, FontIDs> FontHolder;
typedef ResourceHolder<sf::Shader, Shaders> ShaderHolder;
typedef ResourceHolder<sf::SoundBuffer, SoundEffectIDs> SoundBufferHolder;
