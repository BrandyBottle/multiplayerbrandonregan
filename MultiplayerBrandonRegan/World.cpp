#include "World.hpp"
#include "TextNode.hpp"
#include "ParticleNode.hpp"
#include "PostEffect.hpp"
#include "SoundNode.hpp"
#include <SFML/Graphics/RenderTarget.hpp>

#include <algorithm>
#include <cmath>
#include <limits>


World::World(sf::RenderTarget& outputTarget, FontHolder& fonts, SoundPlayer& sounds, int level)
	: mTarget(outputTarget)
	, mSceneTexture()
	, mWorldView(outputTarget.getDefaultView())
	, mTextures()
	, mFonts(fonts)
	, mSounds(sounds)
	, mSceneGraph()
	, mSceneLayers()
	, mWorldBounds(0.f, 0.f, mWorldView.getSize().x, mWorldView.getSize().y)
	, mSpawnPosition(mWorldView.getSize().x / 2.f, mWorldBounds.height - mWorldView.getSize().y / 4.f)
	, mScrollSpeed(-50.f)
	, mBaseballPlayer1(nullptr)
	, mBaseballPlayer2(nullptr)
	, mGameBall(nullptr)
	, mSpeedDisplay(nullptr)
	, mLevel(level)
	, mPlayerDied(false)
{
	mSceneTexture.create(mTarget.getSize().x, mTarget.getSize().y);

	loadTextures();
	buildScene();

	// Prepare the view
	mWorldView.setCenter(512, 393);
}

void World::update(sf::Time dt)
{
	// Forward commands to scene graph, adapt velocity (scrolling, diagonal correction)
	while (!mCommandQueue.isEmpty())
		mSceneGraph.onCommand(mCommandQueue.pop(), dt);

	// Collision detection and response (may destroy entities)
	handleCollisions();

	// Remove all destroyed entities, create new ones
	mSceneGraph.removeWrecks();

	// Regular update step, adapt position (correct if outside view)
	mSceneGraph.update(dt, mCommandQueue);
	adaptBallPosition();
	adaptPlayerPosition();
	updateSounds();
}

void World::draw()
{
		mSceneTexture.clear();
		mSceneTexture.setView(mWorldView);
		mSceneTexture.draw(mSceneGraph);
		mSceneTexture.display();
		mTarget.setView(mWorldView);
		mTarget.draw(mSceneGraph);
}

CommandQueue& World::getCommandQueue()
{
	return mCommandQueue;
}

bool World::hasAlivePlayer() const
{
	return mPlayerDied;
}

void World::loadTextures()
{
	mTextures.load(TextureIDs::BaseballPlayer, "Media/Textures/Player.png");
	mTextures.load(TextureIDs::GameBall, "Media/Textures/Tennisball.png");
	mTextures.load(TextureIDs::Arena, "Media/Textures/Title.png");
	mTextures.load(TextureIDs::Explosion, "Media/Textures/explosionanimation.png");
	mTextures.load(TextureIDs::Particle, "Media/Textures/Particle.png");
}

void World::adaptPlayerPosition()
{
	// Keep player's position inside the screen bounds, at least borderDistance units from the border
	sf::FloatRect viewBounds = getViewBounds();
	const float borderDistance = 32.f;

	sf::Vector2f position = mBaseballPlayer1->getPosition();

	if (position.x < 32)
	{
		mBaseballPlayer1->setPosition(32, position.y);
	}
	else if (position.x > 1024 - borderDistance)
	{
		mBaseballPlayer1->setPosition(1024 - borderDistance, position.y);
	}

	if (position.y <= 48)
	{
		mBaseballPlayer1->setPosition(position.x, 48);
	}
	else if (position.y >= 648)
	{
		mBaseballPlayer1->setPosition(position.x, 648);
	}

	if (mLevel == 2)
	{
		sf::Vector2f position2 = mBaseballPlayer2->getPosition();

			if (position2.x < 32)
			{
				mBaseballPlayer2->setPosition(32, position2.y);
			}
			else if (position2.x > 1024 - borderDistance)
			{
				mBaseballPlayer2->setPosition(1024 - borderDistance, position2.y);
			}

			if (position2.y <= 48)
			{
				mBaseballPlayer2->setPosition(position2.x, 48);
			}
			else if (position2.y >= 648)
			{
				mBaseballPlayer2->setPosition(position2.x, 648);
			}
	}

	
}

void World::adaptBallPosition()
{
	sf::FloatRect viewBounds = getViewBounds();
	const float borderDistance = 32.f;

	sf::Vector2f position = mGameBall->getPosition();
	
	if ((position.x <= 0) || (position.x >= 1024 - borderDistance))
	{
		mGameBall->reverseXDirection();
	}
	if (position.y <= 88 || (position.y >= 648))
	{
 		mGameBall->reverseYDirection();
	}
}

void World::updateSpeedDisplay()
{
	mSpeedDisplay->setString("SPEED " + std::to_string(mGameBall->getSpeed()));
}

bool matchesCategories(SceneNode::Pair& colliders, Category type1, Category type2)
{
	unsigned int category1 = colliders.first->getCategory();
	unsigned int category2 = colliders.second->getCategory();

	// Make sure first pair entry has category type1 and second has type2
	if (static_cast<int>(type1) & category1 && static_cast<int>(type2) & category2)
	{
		return true;
	}
	else if (static_cast<int>(type1) & category2 && static_cast<int>(type2) & category1)
	{
		std::swap(colliders.first, colliders.second);
		return true;
	}
	else
	{
		return false;
	}
}

void World::handleCollisions()
{
	std::set<SceneNode::Pair> collisionPairs;
	mSceneGraph.checkSceneCollision(mSceneGraph, collisionPairs);

	for(SceneNode::Pair pair : collisionPairs)
	{
		if ((matchesCategories(pair, Category::BaseballPlayer, Category::Ball)))
		{
			auto& player = static_cast<BaseballPlayer&>(*pair.first);
			auto& ball = static_cast<GameBall&>(*pair.second);

			if (player.mIsSwinging) 
			{
				if (player.mIsFacingLeft && ball.getPosition().x < player.getPosition().x)
				{
					ball.hit();
					ball.setXDirection(-1);
					updateSpeedDisplay();
				}
				else if (!(player.mIsFacingLeft && ball.getPosition().x > player.getPosition().x))
				{
					ball.hit();
					ball.setXDirection(1);
					updateSpeedDisplay();
				}
				else
				{
					mPlayerDied = true;
					player.defeated();
					ball.remove();
				}	
			}
			else
			{
				mPlayerDied = true;
				player.defeated();
				ball.remove();
			}
			
		}
	}
}

void World::updateSounds()
{
	/*// Set listener's position to player position
	mSounds.setListenerPosition(mBaseballPlayer->getWorldPosition());

	// Remove unused sounds
	mSounds.removeStoppedSounds();*/
}

void World::buildScene()
{
	// Initialize the different layers
	for (std::size_t i = 0; i < Layer::LayerCount; ++i)
	{
		Category category = (i == Layer::LowerAir) ? Category::SceneAirLayer : Category::None;

		SceneNode::Ptr layer(new SceneNode(category));
		mSceneLayers[i] = layer.get();

		mSceneGraph.attachChild(std::move(layer));
	}

	// Prepare the tiled background
	sf::Texture& arenaTexture = mTextures.get(TextureIDs::Arena);
	arenaTexture.setRepeated(true);

	float viewHeight = mWorldView.getSize().y;
	sf::IntRect textureRect(mWorldBounds);
	textureRect.height += static_cast<int>(viewHeight);

	// Add the background sprite to the scene
	std::unique_ptr<SpriteNode> arenaBackground(new SpriteNode(arenaTexture, textureRect));
	arenaBackground->setPosition(mWorldBounds.left, mWorldBounds.top);
	mSceneLayers[Layer::Background]->attachChild(std::move(arenaBackground));

	//Add sound effect node
	std::unique_ptr<SoundNode> soundNode(new SoundNode(mSounds));
	mSceneGraph.attachChild(std::move(soundNode));

	std::unique_ptr<BaseballPlayer> player1(new BaseballPlayer(BaseballPlayer::Type::Player1, mTextures, mFonts, mSounds));
	mBaseballPlayer1 = player1.get();
	mBaseballPlayer1->setPosition(100.f, 368.f);
	mSceneLayers[Layer::UpperAir]->attachChild(std::move(player1));

	if (mLevel == 2)
	{
		std::unique_ptr<BaseballPlayer> player2(new BaseballPlayer(BaseballPlayer::Type::Player2, mTextures, mFonts, mSounds));
		mBaseballPlayer2 = player2.get();
		mBaseballPlayer2->setPosition(924.f, 384.f);
		mBaseballPlayer2->setScale(-1, 1);
		mSceneLayers[Layer::UpperAir]->attachChild(std::move(player2));
	}
	
	// Add exhaust particle node to the scene
	std::unique_ptr<ParticleNode> exhaustNode(new ParticleNode(Particle::Type::Exhaust, mTextures));
	mSceneLayers[UpperAir]->attachChild(std::move(exhaustNode));

	std::unique_ptr<GameBall> gameBall(new GameBall(mTextures, mFonts, mSounds));
	mGameBall = gameBall.get();
	mGameBall->setPosition(sf::Vector2f(512.f, 384.f));
	mSceneLayers[Layer::UpperAir]->attachChild(std::move(gameBall));

	std::unique_ptr<TextNode> speedDisplay(new TextNode(mFonts, "SPEED: " + std::to_string(mGameBall->getSpeed())));
	mSpeedDisplay = speedDisplay.get();
	mSpeedDisplay->setPosition(512.f, 55.f);
	mSpeedDisplay->setColor(sf::Color::Black);
	mSceneLayers[Layer::UpperAir]->attachChild(std::move(speedDisplay));
	
}


sf::FloatRect World::getViewBounds() const
{
	return sf::FloatRect(mWorldView.getCenter() - mWorldView.getSize() / 2.f, mWorldView.getSize());
}

