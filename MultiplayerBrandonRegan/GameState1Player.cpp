#include "GameState1Player.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include "MusicPlayer.hpp"


GameState1Player::GameState1Player(StateStack& stack, Context context)
	:State(stack, context)
	, mWorld(*context.window, *context.fonts, *context.sounds, 1)
	, mPlayer1(*context.player1)
	, mPlayer2(*context.player2)
{

	//Play the mission theme
	context.music->play(MusicIDs::MissionTheme);
}

void GameState1Player::draw()
{
	mWorld.draw();
}

bool GameState1Player::update(sf::Time dt)
{
	mWorld.update(dt);

	if (mWorld.hasAlivePlayer())
	{
		mDeathStateDelay += dt;
		if (mDeathStateDelay > sf::seconds(2))
		{
			requestStackPush(StateIDs::GameOver);
		}
	}

	else
	{
		CommandQueue& commands = mWorld.getCommandQueue();
		mPlayer1.handleRealtimeInput(commands);
		mPlayer2.handleRealtimeInput(commands);
	}


	return true;
}

bool GameState1Player::handleEvent(const sf::Event & event)
{
	//Game input handling
	CommandQueue& commands = mWorld.getCommandQueue();
	mPlayer1.handleEvent(event, commands);
	mPlayer2.handleEvent(event, commands);

	//Escape pressed, trigger pause screen
	if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
	{
		requestStackPush(StateIDs::Pause);
	}
	// Add a command to player two in future that does this...
	// Player 2 joins
	else if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Enter)
	{
		requestStackPush(StateIDs::Player2Join);
	}
	return true;
}


