#pragma once
#include "State.hpp"
#include "Container.hpp"
#include "SoundPlayer.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


class ChallengerApproachingState : public State
{
public:
	ChallengerApproachingState(StateStack& stack, Context context);

	virtual void		draw();
	virtual bool		update(sf::Time dt);
	virtual bool		handleEvent(const sf::Event& event);


private:
	sf::Text			mText;
	sf::Time			mElapsedTime;
	SoundPlayer&		mSounds;
};