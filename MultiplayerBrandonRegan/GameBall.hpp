#pragma once
#include "Entity.hpp"
#include "ResourceIdentifiers.hpp"
#include "TextNode.hpp"
#include "SoundPlayer.hpp"

#include "SFML/Graphics/Sprite.hpp"

class GameBall : public Entity
{
public:
	GameBall(const TextureHolder& textures, const FontHolder& fonts, SoundPlayer& sounds);
	virtual sf::FloatRect getBoundingRect() const;
	virtual unsigned int getCategory() const;
	int getSpeed();
	void increaseSpeed();
	void reverseXDirection();
	void reverseYDirection();
	void setXDirection(float x);
	void hit();	
	void remove();

	int mSpeed;
	bool isHit;

private:
	virtual void updateCurrent(sf::Time dt, CommandQueue& commands);
	virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	void checkHitInterval(sf::Time dt);


private:

	sf::Sprite mSprite;
	sf::Vector2f mDirection;
	sf::Time mHitBuffer;
	SoundPlayer& mSounds;

};