#pragma once
#include "Entity.hpp"
#include "ResourceIdentifiers.hpp"
#include "Command.hpp"
#include "TextNode.hpp"
#include "SoundPlayer.hpp"
#include "Animation.hpp"

#include "SFML/Graphics/Sprite.hpp"

class BaseballPlayer : public Entity
{
public:
	enum class Type { Player1, Player2, TypeCount };

public:
	BaseballPlayer(Type type, const TextureHolder& texture, const FontHolder& fonts, SoundPlayer& sounds);
	virtual unsigned int getCategory() const;
	virtual sf::FloatRect getBoundingRect() const;
	void defeated();

	bool mIsFacingLeft;
	bool mIsSwinging;	
	bool mDefeated;

	void swing();
	void playSwingSound(CommandQueue& commands);
	void playLocalSound(CommandQueue& commands, SoundEffectIDs effect);

private:
	virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual void updateCurrent(sf::Time dt, CommandQueue& commands);
	void updateTexts();
	void checkSwingInterval(sf::Time dt);

private:
	Type mType;
	sf::Sprite mSprite;
	Animation mExplosion;
	sf::Time mSwingCooldown;
	bool mShowExplosion;
	bool mPlayedExplosionSound;
	bool mPlayedSwingSound;
	SoundPlayer& mSounds;

	TextNode* mPlayerIndicator;
};