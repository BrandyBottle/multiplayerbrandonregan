#include "Player1ControlState.hpp"

#include "Utility.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

Player1ControlState::Player1ControlState(StateStack & stack, Context context)
	: State(stack, context)
	, mGUIContainer()
	, mSettingsBox()
{
	sf::Texture& texture = context.textures->get(TextureIDs::MenuScreen);
	texture.setRepeated(true);
	mBackgroundSprite.setTexture(texture);

	// Tiles the background image to fill the screen
	mBackgroundSprite.setTextureRect(sf::IntRect(0, 0, 1500, 768));

	mSettingsBox.setFillColor(sf::Color(173, 216, 230));
	mSettingsBox.setOutlineColor(sf::Color(255, 255, 255));
	mSettingsBox.setOutlineThickness(2.f);
	mSettingsBox.setSize(sf::Vector2f(964, 708));
	mSettingsBox.setPosition(30, 30);

	addButtonLabel(Player::Action::MoveLeft, 50.f, "Move Left", context);
	addButtonLabel(Player::Action::MoveRight, 150.f, "Move Right", context);
	addButtonLabel(Player::Action::MoveUp, 250.f, "Move Up", context);
	addButtonLabel(Player::Action::MoveDown, 350.f, "Move Down", context);
	addButtonLabel(Player::Action::Swing, 450.f, "Swing", context);

	updateLabels();

	auto backButton = std::make_shared<GUI::Button>(context);
	backButton->setPosition(384.f, 550.f);
	backButton->setText("Back");
	backButton->setCallback(std::bind(&Player1ControlState::requestStackPop, this));

	mGUIContainer.pack(backButton);

}

void Player1ControlState::draw()
{
	sf::RenderWindow& window = *getContext().window;

	window.draw(mBackgroundSprite);
	window.draw(mSettingsBox);
	window.draw(mGUIContainer);
}

bool Player1ControlState::update(sf::Time dt)
{
	if (mBackgroundSprite.getPosition().x <= -100)
	{
		mBackgroundSprite.setPosition(0, 0);
	}
	mBackgroundSprite.setPosition(mBackgroundSprite.getPosition() + sf::Vector2f(-1, 0));
	return false;
}

bool Player1ControlState::handleEvent(const sf::Event& event)
{
	bool isKeyBinding = false;

	// Iterate through all key binding buttons to see if they are being pressed, waiting for the user to enter a key
	for (std::size_t action = 0; action < static_cast<int>(Player::Action::ActionCount); ++action)
	{
		if (mBindingButtons[action]->isActive())
		{
			isKeyBinding = true;
			if (event.type == sf::Event::KeyReleased)
			{
				getContext().player1->assignKey(static_cast<Player::Action>(action), event.key.code);
				mBindingButtons[action]->deactivate();
			}
			break;
		}
	}

	// If pressed button changed key bindings, update labels; otherwise consider other buttons in container
	if (isKeyBinding)
		updateLabels();
	else
		mGUIContainer.handleEvent(event);

	return false;
}

void Player1ControlState::updateLabels()
{
	Player& player = *getContext().player1;

	for (std::size_t i = 0; i < static_cast<int>(Player::Action::ActionCount); ++i)
	{
		sf::Keyboard::Key key = player.getAssignedKey(static_cast<Player::Action>(i));
		mBindingLabels[i]->setText(toString(key));
	}
}

void Player1ControlState::addButtonLabel(Player::Action action, float y, const std::string& text, Context context)
{
	mBindingButtons[static_cast<int>(action)] = std::make_shared<GUI::Button>(context);
	mBindingButtons[static_cast<int>(action)]->setPosition(384.f, y);
	mBindingButtons[static_cast<int>(action)]->setText(text);
	mBindingButtons[static_cast<int>(action)]->setToggle(true);

	mBindingLabels[static_cast<int>(action)] = std::make_shared<GUI::Label>("", *context.fonts);
	mBindingLabels[static_cast<int>(action)]->setPosition(700.f, y);
	mBindingLabels[static_cast<int>(action)]->setTextColor(sf::Color::Black);


	mGUIContainer.pack(mBindingButtons[static_cast<int>(action)]);
	mGUIContainer.pack(mBindingLabels[static_cast<int>(action)]);
}

