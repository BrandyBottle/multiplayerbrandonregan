#pragma once
enum class StateIDs {
	None,
	Title,
	Menu,
	Game1,
	Game2,
	Settings,
	Pause,
	GameOver,
	LevelSelect,
	Player2Join,
	ControlSelect,
	Player1Controls,
	Player2Controls,
};