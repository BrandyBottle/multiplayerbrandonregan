#include "ControlSelectState.hpp"

#include "Utility.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

ControlSelectState::ControlSelectState(StateStack & stack, Context context)
	: State(stack, context)
	, mGUIContainer()
	, mSelectionBox()
{
	sf::Texture& texture = context.textures->get(TextureIDs::MenuScreen);
	texture.setRepeated(true);
	mBackgroundSprite.setTexture(texture);

	// Tiles the background image to fill the screen
	mBackgroundSprite.setTextureRect(sf::IntRect(0, 0, 1500, 768));

	mSelectionBox.setFillColor(sf::Color(173, 216, 230));
	mSelectionBox.setOutlineColor(sf::Color(255, 255, 255));
	mSelectionBox.setOutlineThickness(2.f);
	mSelectionBox.setSize(sf::Vector2f(964, 708));
	mSelectionBox.setPosition(30, 30);

	auto game1Button = std::make_shared<GUI::Button>(context);
	game1Button->setPosition(384, 150);
	game1Button->setText("Player 1 Setup");
	game1Button->setCallback([this]()
	{
		requestStackPop();
		requestStackPush(StateIDs::Player1Controls);
	});

	auto game2Button = std::make_shared<GUI::Button>(context);
	game2Button->setPosition(384, 300);
	game2Button->setText("Player 2 Setup");
	game2Button->setCallback([this]()
	{
		requestStackPop();
		requestStackPush(StateIDs::Player2Controls);
	});

	auto menuButton = std::make_shared<GUI::Button>(context);
	menuButton->setPosition(384, 450);
	menuButton->setText("Back");
	menuButton->setCallback([this]()
	{
		requestStackPop();
		requestStackPush(StateIDs::Menu);
	});

	mGUIContainer.pack(game1Button);
	mGUIContainer.pack(game2Button);
	mGUIContainer.pack(menuButton);
}

void ControlSelectState::draw()
{
	sf::RenderWindow& window = *getContext().window;

	window.draw(mBackgroundSprite);
	window.draw(mSelectionBox);
	window.draw(mGUIContainer);
}

bool ControlSelectState::update(sf::Time dt)
{
	if (mBackgroundSprite.getPosition().x <= -100)
	{
		mBackgroundSprite.setPosition(0, 0);
	}
	mBackgroundSprite.setPosition(mBackgroundSprite.getPosition() + sf::Vector2f(-1, 0));
	return false;
}

bool ControlSelectState::handleEvent(const sf::Event & event)
{
	mGUIContainer.handleEvent(event);
	return false;
}
