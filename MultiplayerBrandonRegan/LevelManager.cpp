#include "LevelManager.hpp"

LevelManager::LevelManager()
	:currentLevel(Level::notSet)
{

}

int LevelManager::getCurrentLevel()
{
	return static_cast<int>(currentLevel);
}

void LevelManager::setLevel(int level)
{
	Level levelSelected = static_cast<Level>(level);
	currentLevel = levelSelected;
}
