#include "DataTables.hpp"
#include "BaseballPlayer.hpp"
#include "GameBall.hpp"
#include "Particle.hpp"
#include "Constants.hpp"

using namespace std::placeholders;

std::vector<ParticleData> initializeParticleData()
{
	std::vector<ParticleData> data(static_cast<int>(Particle::Type::ParticleCount));
	data[static_cast<int>(Particle::Type::Exhaust)].color = sf::Color(255, 255, 50);
	data[static_cast<int>(Particle::Type::Exhaust)].lifetime = sf::seconds(0.6f);

	data[static_cast<int>(Particle::Type::Smoke)].color = sf::Color(50, 50, 50);
	data[static_cast<int>(Particle::Type::Smoke)].lifetime = sf::seconds(4.0);

	return data;
}

std::vector<BaseballPlayerData> initializeBaseballPlayerData()
{
	std::vector<BaseballPlayerData> data(static_cast<int>(BaseballPlayer::Type::TypeCount));
	data[static_cast<int>(BaseballPlayer::Type::Player1)].speed = 200.f;
	data[static_cast<int>(BaseballPlayer::Type::Player1)].swingInterval = sf::seconds(.25f);
	data[static_cast<int>(BaseballPlayer::Type::Player1)].hitpoints = 2;
	data[static_cast<int>(BaseballPlayer::Type::Player1)].texture = TextureIDs::BaseballPlayer;
	data[static_cast<int>(BaseballPlayer::Type::Player1)].textureRect = sf::IntRect(5, 5, 17, 24);

	data[static_cast<int>(BaseballPlayer::Type::Player2)].speed = 200.f;
	data[static_cast<int>(BaseballPlayer::Type::Player2)].swingInterval = sf::seconds(.25f);
	data[static_cast<int>(BaseballPlayer::Type::Player2)].hitpoints = 2;
	data[static_cast<int>(BaseballPlayer::Type::Player2)].texture = TextureIDs::BaseballPlayer;
	data[static_cast<int>(BaseballPlayer::Type::Player2)].textureRect = sf::IntRect(66, 5, 17, 24);
	
	return data;
}

std::vector<GameBallData> initializeGameBallData()
{
	std::vector<GameBallData> data(1);

	data[0].speed = 150.f;
	data[0].texture = TextureIDs::GameBall;
	data[0].hitDelay = sf::seconds(.50f);

	return data;
}

