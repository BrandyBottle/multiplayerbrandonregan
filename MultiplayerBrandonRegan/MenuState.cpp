#include "MenuState.hpp"
#include "Button.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"
#include "MusicPlayer.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/RectangleShape.hpp>


MenuState::MenuState(StateStack& stack, Context context)
	: State(stack, context)
	, mGUIContainer()
	, mButtonBackground()
{
	sf::Texture& texture = context.textures->get(TextureIDs::MenuScreen);
	texture.setRepeated(true);
	mBackgroundSprite.setTexture(texture);

	// Tiles the background image to fill the screen
	mBackgroundSprite.setTextureRect(sf::IntRect (0, 0, 1500, 768));

	mButtonBackground.setFillColor(sf::Color(173, 216, 230));
	mButtonBackground.setOutlineColor(sf::Color(255, 255, 255));
	mButtonBackground.setOutlineThickness(2.f);
	mButtonBackground.setSize(sf::Vector2f(400, 500));
	mButtonBackground.setPosition(52, 90);
	
	sf::Texture& texture1 = context.textures->get(TextureIDs::MenuLogo);

	mLogo.setPosition(750, 334);
	mLogo.setTexture(texture1);
	mLogo.scale(sf::Vector2f(6, 6));
	centreOrigin(mLogo);

	auto playButton = std::make_shared<GUI::Button>(context);
	playButton->setPosition(100, 150);
	playButton->setText("Play");
	playButton->setCallback([this]()
	{
		requestStackPop();
		requestStackPush(StateIDs::LevelSelect);
	});

	auto settingsButton = std::make_shared<GUI::Button>(context);
	settingsButton->setPosition(100, 300);
	settingsButton->setText("Settings");
	settingsButton->setCallback([this]()
	{
		requestStackPush(StateIDs::ControlSelect);
	});

	auto exitButton = std::make_shared<GUI::Button>(context);
	exitButton->setPosition(100, 450);
	exitButton->setText("Exit");
	exitButton->setCallback([this]()
	{
		requestStackPop();
	});

	mGUIContainer.pack(playButton);
	mGUIContainer.pack(settingsButton);
	mGUIContainer.pack(exitButton);

	//Play the menu music
	context.music->play(MusicIDs::MenuTheme);
}

void MenuState::draw()
{
	sf::RenderWindow& window = *getContext().window;

	window.setView(window.getDefaultView());



	window.draw(mBackgroundSprite);
	window.draw(mButtonBackground);
	window.draw(mLogo);
	window.draw(mGUIContainer);
}

bool MenuState::update(sf::Time)
{
	if (mBackgroundSprite.getPosition().x <= -100)
	{
		mBackgroundSprite.setPosition(0,0);
	}
	mBackgroundSprite.setPosition(mBackgroundSprite.getPosition() + sf::Vector2f(-1, 0));
	return true;
}

bool MenuState::handleEvent(const sf::Event& event)
{
	mGUIContainer.handleEvent(event);
	return false;
}
