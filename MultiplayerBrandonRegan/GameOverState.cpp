#include "GameOverState.hpp"
#include "Utility.hpp"
#include "Player.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>


GameOverState::GameOverState(StateStack& stack, Context context)
	: State(stack, context)
	, mGameOverText1()
	, mGameOverText2()
	, mElapsedTime(sf::Time::Zero)
	, mGameOverBack1()
	, mGameOverBack2()
	, mSounds(*context.sounds)
	, mKOSoundPlayed(false)
{
	sf::Font& font = context.fonts->get(FontIDs::Main);
	sf::Vector2f windowSize(context.window->getSize());

	// Create dark, semitransparent background
	mGameOverBack1.setFillColor(sf::Color(0, 0, 0, 150));
	mGameOverBack1.setSize(sf::Vector2f(512.f, 200.f));
	mGameOverBack1.setOrigin(sf::Vector2f(0, 200.f / 2));
	mGameOverBack1.setPosition(-512, 344);

	mGameOverBack2.setFillColor(sf::Color(0, 0, 0, 150));
	mGameOverBack2.setSize(sf::Vector2f(512.f, 200.f));
	mGameOverBack2.setOrigin(sf::Vector2f(0, 200.f / 2));
	mGameOverBack2.setPosition(1024, 344);

	mGameOverText1.setFont(font);
	mGameOverText1.setString("K");
	centreOrigin(mGameOverText1);
	mGameOverText1.setCharacterSize(70);
	mGameOverText1.setPosition(-60, 320);

	mGameOverText2.setFont(font);
	mGameOverText2.setString("O!");
	centreOrigin(mGameOverText2);
	mGameOverText2.setCharacterSize(70);
	mGameOverText2.setPosition(1046, 320);

}

void GameOverState::draw()
{
	sf::RenderWindow& window = *getContext().window;
	window.setView(window.getDefaultView());

	window.draw(mGameOverBack1);
	window.draw(mGameOverBack2);
	window.draw(mGameOverText1);
	window.draw(mGameOverText2);
}

bool GameOverState::update(sf::Time dt)
{
	mElapsedTime += dt;
	if (mElapsedTime > sf::seconds(5))
	{
		requestStackClear();
		requestStackPush(StateIDs::LevelSelect);
	}
	if (mGameOverBack1.getPosition().x < 0)
	{
		mGameOverBack1.setPosition(mGameOverBack1.getPosition() + sf::Vector2f(8, 0));
		mGameOverText1.setPosition(mGameOverText1.getPosition() + sf::Vector2f(8, 0));
	}
	if (mGameOverBack2.getPosition().x > 512)
	{
		mGameOverBack2.setPosition(mGameOverBack2.getPosition() + sf::Vector2f(-8, 0));
		mGameOverText2.setPosition(mGameOverText2.getPosition() + sf::Vector2f(-8, 0));
	}
	else
	{
		if (!mKOSoundPlayed)
		{
			mSounds.play(SoundEffectIDs::KO);
			mKOSoundPlayed = true;
		}
	}
	return false;
}

bool GameOverState::handleEvent(const sf::Event& event)
{
	if (event.key.code == sf::Keyboard::R)
	{
		requestStackClear();
		requestStackPush(StateIDs::LevelSelect);
	}

	if (event.key.code == sf::Keyboard::M)
	{
		requestStackClear();
		requestStackPush(StateIDs::Menu);
	}
	return false;
}
